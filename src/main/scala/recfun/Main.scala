package recfun
import common._

object Main {
  def main(args: Array[String]) {
    println("Pascal's Triangle")
    for (row <- 0 to 10) {
      for (col <- 0 to row)
        print(pascal(col, row) + " ")
      println()
    }
  }

  /**
   * Exercise 1
   */
  def pascal(c: Int, r: Int): Int = (c,r) match { 
    case (0,_) | (_, 0) | (_, 1) => 1
    case (c,r) if (r == c) => 1
    case _ => pascal(c-1, r-1) + pascal(c, r-1)
  }

  /**
   * Exercise 2
   */
/*  def balance2(chars: List[Char]): Boolean = { 
    def computeBalanced(c:Char):Int => Option[Int] = c match { 
      case '(' => i => Some(i+1)
      case ')' => i => Some(i-1) filter(_ =>(i-1) >= 0)
      case _ => i => Some(i)
    } 
    val isBalanced = chars.foldLeft(Some(0):Option[Int])((a,b) => 
      a flatMap computeBalanced(b))

    isBalanced.isDefined
  }*/

  def balance(chars:List[Char]): Boolean = { 
    def _balance(chars:List[Char], res:Int):Boolean = 
      if (chars.isEmpty) (res == 0)
      else chars.head match { 
        case ')' if res == 0 => false
        case ')' => _balance(chars.tail, res-1)
        case '(' => _balance(chars.tail, res+1)
        case _   => _balance(chars.tail, res)
    }
    _balance(chars, 0)
  }

  /**
   * Exercise 3
   */
  def countChange(money: Int, coins: List[Int]): Int = { 
    def applyCount(n:Int) = (l:List[Int]) => l match { 
      case Nil => 0
      case l@(x::xs) => { 
        countChange(n-x, l filter (c => c <= n && c > 0))
      }
    }
    if (money == 0) 1
    else coins match { 
      case Nil => 0
      case list@(x::xs) => list.tails.map(applyCount(money)(_)).sum
    }
  }
}
